import sys
import socket

from scapy.supersocket import StreamSocket
import scapy.contrib.scada.iec104 as iec104


class IEC104Client:
    """
    A scapy-based IEC 60870-5-104 client that can send packets to a given IEC 60870-5-104 server.

    The IEC104Client handles the connection to a server and also adjusts and sets the IEC 60870-5-101/104 sequence numbers
    according to the standard, because otherwise some servers might just ignore messages or improperly close connections.
    """

    def __init__(self, ip: str, port: int):
        """
        Initialize IEC104Client.

        :param ip: IP address of server to connect to.
        :param port: Destination port number on server.
        """
        self.__ip = ip
        self.__port = port
        self.__client = None

        self.__next_tx_seq_num = 0
        self.__next_rx_seq_num = 0

    def __enter__(self):
        """
        Implement Python function __enter__ to support with statements.
        """
        self.connect()
        return self

    def __exit__(self, *args):
        """
        Implement Python function __exit__ to support with statements (and properly close the connection afterwards).
        """
        self.disconnect()

    def connect(self):
        """
        Connect to server / port specified at class instantiation.
        """
        # only connect if it isn't connected, yet
        if self.__client:
            return

        sock = socket.socket()
        try:
            sock.connect((self.__ip, self.__port))
        except ConnectionRefusedError:
            print(
                f"Server not reachable. Make sure it's running and available at given address: {self.__ip}:{self.__port}"
            )
            sys.exit(1)
        self.__client = StreamSocket(sock)

    def disconnect(self):
        """
        Close connection to server.
        """
        if self.__client:
            self.__client.close()

    def send(self, pkt: iec104.IEC104_APDU):
        """
        Send a given IEC 60870-5-104 scapy packet to server.

        It adjusts and fixes the IEC 60870-5-101/104 sequence numbers automatically according to the standard,
        because otherwise some servers might just ignore messages or improperly close connections.

        :param pkt: IEC 60870-5-104 scapy packet.
        """
        if not self.__client:
            print("Please connect to a server before sending a packet!")
            return

        print("======== Sending packet ========")
        pkt.show2()
        print("========")

        # For IEC 104 types that have a sequence number: Adjust it correctly.
        if issubclass(type(pkt), iec104.IEC104_I_Message) or issubclass(type(pkt), iec104.IEC104_S_Message):
            pkt.tx_seq_num = self.__next_tx_seq_num
            pkt.rx_seq_num = self.__next_rx_seq_num
            print(f"SEND seq no TX: {pkt.tx_seq_num} / RX: {pkt.rx_seq_num} / Type: {type(pkt).__name__}")
            self.__next_tx_seq_num += 1

        ans, _ = self.__client.sr(pkt, inter=0.2, timeout=0.4)  # send packet and receive answer-packets

        for a in ans:
            ans_pkt = iec104.iec104_decode(bytes(a.answer))

            print("======== Received answering packet ========")
            ans_pkt.show2()
            print("========")

            if issubclass(type(ans_pkt), iec104.IEC104_I_Message) or issubclass(type(ans_pkt), iec104.IEC104_S_Message):
                print(
                    f"RECV seq no TX: {ans_pkt.tx_seq_num} / RX: {ans_pkt.rx_seq_num} / Type: {type(ans_pkt).__name__}"
                )
                self.__next_rx_seq_num += 1
