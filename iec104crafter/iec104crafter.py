import scapy.contrib.scada.iec104 as iec104


class IEC104Crafter:
    """
    The IEC104Craft class contains methods to build different IEC 60870-5-104 scapy packets.

    You do not need to instantiate this class as it only is a container for multiple staticmethods.
    """

    @staticmethod
    def build_connection_cmd(
        testfr_con: int = 0,
        testfr_act: int = 0,
        stopdt_con: int = 0,
        stopdt_act: int = 0,
        startdt_con: int = 0,
        startdt_act: int = 0,
    ):
        """
        Craft connection command like STARTDT ACT which initialises a connection in IEC60870-5-101 and 104 (u-message).

        Can also be used to build all other possible U-type-messages. E.g. STARTDT ACT must be sent at the start of a new connection.
        According to the standard, only one of the values is supposed to be 1 at a time. But it's possible to try other combinations for testing purposed, e.g. fuzzing.

        :param testfr_con: Set to 1 for testfr confirmation.
        :param testfr_act: Set to 1 for testfr activation.
        :param stopdt_con: Set to 1 for stopdt confirmation.
        :param stopdt_act: Set to 1 for stopdt activation.
        :param startdt_con: Set to 1 for startdt confirmation.
        :param startdt_act: Set to 1 for startdt activation.
        """
        # U-message STARTDT ACT (init connection)
        startdt_act_cmd = iec104.IEC104_U_Message()
        startdt_act_cmd.testfr_con = testfr_con  # confirm
        startdt_act_cmd.testfr_act = testfr_act  # activate
        startdt_act_cmd.stopdt_con = stopdt_con  # confirm
        startdt_act_cmd.stopdt_act = stopdt_act  # activate
        startdt_act_cmd.startdt_con = startdt_con  # confirm
        startdt_act_cmd.startdt_act = startdt_act  # activate

        return startdt_act_cmd

    @staticmethod
    def build_general_interrogation_cmd(cot: int = iec104.IEC104_COT_ACT, ca: int = 65535, ioa: int = 0, qoi: int = 20):
        """
        Craft an IEC60870-5-104 C_IC_NA_1 (general interrogation) command (i-message).

        :param cot: Cause of transaction (default: ACT).
        :param ca: Common address (default: 65535 = broadcast (only available for certain types like C_IC_NA_1)).
        :param ioa: IO address.
        :param qoi: Qualifier of interrogation (default: 20 = global station interrogation).
        """
        # I-message C_IC_NA_1 ACT (start connection)
        start_con_cmd = iec104.IEC104_I_Message_SingleIOA()

        # populate fields
        start_con_cmd.type_id = iec104.IEC104_IO_ID_C_IC_NA_1
        start_con_cmd.cot = cot
        start_con_cmd.common_asdu_address = ca

        # populate payload
        start_con_cmd_payload = iec104.IEC104_IO_C_IC_NA_1_IOA()
        start_con_cmd_payload.information_object_address = ioa
        start_con_cmd_payload.qoi = qoi

        start_con_cmd.io = start_con_cmd_payload
        return start_con_cmd

    @staticmethod
    def build_set_point_cmd(cot: int = iec104.IEC104_COT_ACT, ca: int = 1, ioa: int = 1, value: float = 1.0):
        """
        Craft an IEC60870-5-104 C_SE_NC_1 (set point short floating point number) command (i-message).

        :param cot: Cause of transaction (default: ACT).
        :param ca: Common address.
        :param ioa: IO address.
        :param value: Float value of point.
        """
        set_point_cmd = iec104.IEC104_I_Message_SingleIOA()

        # populate fields
        set_point_cmd.type_id = iec104.IEC104_IO_ID_C_SE_NC_1
        set_point_cmd.cot = cot
        set_point_cmd.common_asdu_address = ca

        # populate payload
        set_point_cmd_payload = iec104.IEC104_IO_C_SE_NC_1_IOA()
        set_point_cmd_payload.information_object_address = ioa
        set_point_cmd_payload.scaled_value = value

        set_point_cmd.io = set_point_cmd_payload

        return set_point_cmd

    @staticmethod
    def build_double_cmd(cot: int = iec104.IEC104_COT_ACT, ca: int = 0, ioa: int = 0, dcs_value: int = 1):
        """
        Craft an IEC60870-5-104 C_DC_NA_1 (double) command (i-message).

        :param cot: Cause of transaction (default: ACT).
        :param ca: Common address.
        :param ioa: IO address.
        :param dcs_value: Possible values: 0 = invalid / 1 = off / 2 = on / 3 = invalid (default: 1 = off)
        """
        if dcs_value not in range(0, 4):
            # as there are only 2 bit for this value; but actually as per standard, only 1 and 2 are allowed numbers
            raise ValueError("dcs_value must be in range(0, 4)")

        double_cmd = iec104.IEC104_I_Message_SingleIOA()

        # populate fields
        double_cmd.type_id = iec104.IEC104_IO_ID_C_DC_NA_1
        double_cmd.cot = cot
        double_cmd.common_asdu_address = ca

        # populate payload
        double_cmd_payload = iec104.IEC104_IO_C_DC_NA_1_IOA()
        double_cmd_payload.information_object_address = ioa
        double_cmd_payload.dcs = dcs_value

        double_cmd.io = double_cmd_payload

        return double_cmd
