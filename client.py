#!/usr/bin/env python3

"""
Example file that shows how to craft IEC 60870-5-104 packets and how to use them as a client and send them to server.

Notes, tips and tricks for working with Scapy, despite the sometimes thin documentation (https://scapy.readthedocs.io/en/latest/index.html):
- To start the scapy shell, run "sudo scapy -H".
- To explore available data fields, type "explore()" in scapy shell.
- To examine a hexdump of a packet, use "hexdump(my_packet)".
- To see all possible fields of a data type, use something like "IEC104_U_Message().show()" and ".show2()"
"""

from iec104crafter.iec104client import IEC104Client
from iec104crafter.iec104crafter import IEC104Crafter

iec104_server_ip = "127.0.0.1"
iec104_server_port = 2404

with IEC104Client(iec104_server_ip, iec104_server_port) as client:
    # STARTDT ACT
    startdt_act_cmd = IEC104Crafter.build_connection_cmd(startdt_act=1)
    client.send(startdt_act_cmd)

    # C_IC_NA_1 ACT
    general_interrogation_cmd = IEC104Crafter.build_general_interrogation_cmd()  # global general interrogation command
    client.send(general_interrogation_cmd)

    # C_SE_NC_1 ACT
    set_point_cmd = IEC104Crafter.build_set_point_cmd(ca=1, ioa=2, value=1234.56)  # set point to value 1234.56
    client.send(set_point_cmd)

    # C_DC_NA_1 ACT
    double_cmd = IEC104Crafter.build_double_cmd(ca=1, ioa=3, dcs_value=1)  # double cmd with dcs_value = 1 = off
    client.send(double_cmd)
