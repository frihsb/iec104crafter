# IEC104Crafter

Craft and send IEC 60870-5-104 packets with this scapy-based toolset. The idea is that you can craft packets that fit exactly your needs. "Crafting" a packet means, that you can create a packet from ground up with full control over all of the fields and attributes, so you can send standard conform and non-conform IEC 60870-5-104 packets for whichever testing purposed (but please be considerate and only use this for good purposes!).

## Getting started

Clone the repository and follow the installation steps in the next section.

## Installation

For best portability (at least Ubuntu and MacOS), the script is run inside a docker container. Thus, make sure to have the following dependencies installed:

* `docker-compose`
* `Docker`

## Usage

You will need a target sever that can be used to connect to. Unfortunately, we can't provide you with any server implementation for testing purposes, but trying [Conpot](https://github.com/mushorg/conpot) as a target "server" might be an option as it also provides an IEC 60870-5-104 implementation.

Start the `IEC104Client` with:

```bash
docker-compose build
docker-compose up
```

This makes use of the `host` network in Docker, so the script behaves (from a networking standpoint) as if it is run directly on your computer and not inside a Docker container. To adapt this to your own needs, have a look at the `client.py` which gives a few examples.

To open a client-server connection, use Python's `with`-statement. This way it can be ensured that the socket is closed after the connection:

```python
iec104_server_ip = "127.0.0.1"
iec104_server_port = 2404

with IEC104Client(iec104_server_ip, iec104_server_port) as client:
    # ... do some stuff here with the client
    pass
```

Currently, only the following message types are implemented:

* U-Messages with all possible flags, e.g. `STARTDT ACT` (but also TESTFR and STOPTDT ACT and CON) commands
* `C_IC_NA_1` (interrogation command)
* `C_SE_NC_1` (set point short floating point number command)
* `C_DC_NA_1` (double command)

You can build and send these commands like this:

```python
# STARTDT ACT
startdt_act_cmd = IEC104Crafter.build_connection_cmd(startdt_act = 1)
client.send(startdt_act_cmd)
```

Please note that you normally always need to send a `STARTDT ACT` command to the server at the start of a connection. For a full example, including the other IEC 60870-5-104 data types, have a look at the `client.py`-file from this repository.

## Contributing
Feel free to contribute with a merge request! Just make sure to use PEP8 coding standards and have a look at the existing code. Use docstrings and typehints (at least in function signatures).

Possible contributions would e.g. be implementations of other IEC 60870-5-104 types than the ones already implemented. It's pretty straight forward and mostly just copy-paste and adjusting the methods to the different types and attributes (and of course documenting them).

That said, we use a CI pipeline that uses multiple tools such as `mypy`, `flask8`, `black` and others to ensure that only nice code is added to the repository. For convenience reasons, we use pre-commit-hooks. For development, we also suggest using a virtual environment:

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Then, after having created and activated the venv, install the pre-commit hooks:

```bash
# Install pre-commit Hooks
pre-commit install --install-hooks --overwrite

# Check all files for conformity
pre-commit run --all-files

# In case you want to uninstall the pre-commit hooks again for whichever reason, run:
# pre-commit uninstall
```

## Authors
The toolset is based on work by (ordered alphabetically by the first names):

- Fabian Niehaus: Forschungsgruppe Rechnernetze und Informationssicherheit (FRI), Hochschule Bremen (City University of Applied Sciences)
- Torben Woltjen: Forschungsgruppe Rechnernetze und Informationssicherheit (FRI), Hochschule Bremen (City University of Applied Sciences)

## License
As this toolset is based on scapy, it is also licensed under GPLv2 (see LICENSE file for the complete license text).

## Project status
This project is not necessarily actively maintained. It was published because there were only very few examples online on how to use scapy to build IEC 60870-5-104 packets and maybe it comes in handy for you! You can try to open a merge request and it might be merged, but there's no guarantee :)
