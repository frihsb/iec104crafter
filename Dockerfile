FROM ubuntu:20.04
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /files/app

RUN apt-get update && apt-get install -y \
    python3-pip \
    net-tools \
    iproute2 \
    tcpdump \
    iputils-ping \
    procps \
    grep \
    less \
    curl \
    wget \
    iperf3 \
    traceroute \
    && rm -rf /var/lib/apt/lists/*

COPY ./requirements.txt /files/requirements.txt
COPY ./iec104crafter /files/app/iec104crafter
RUN pip3 install -r /files/requirements.txt && rm /files/requirements.txt
